#include "Snake.h"
#include <iostream>

using namespace std;

// Creates a snake actor, and updates it's size.
Snake::Snake()
{
	bounds = 16;

	direction = 'd';

	for (int i = 0; i < 3; i++)
	{
		//use the {} to construct a pair within the push_back method
		pos.push_back({ 8, 8 - i });
	}

	alive = true;
}

// Creates a snake actor with a custom size and starting location.
Snake::Snake(int x, int y)
{
	bounds = 16;

	if (x < 1){
		x = 1;
	}
	else if(x > 16){
		x = 16;
	}
	if (y < 3){
		y = 3;
	}
	else if (y > 16){
		y = 16;
	}

	direction = 'd';

	for (int i = 0; i < 3; i++)
	{
		pos.push_back({ x, y - i });
	}

	alive = true;
}

// Copy Constructor
// "this" references the new object, and allows its values to be assigned by the constructors argument object "s".
Snake& Snake::operator=(const Snake& s)
{
	this->bounds = s.bounds;

	this->direction = s.direction;

	this->pos.clear();

	for (int i = 0; i < s.pos.size(); i++)
	{
		this->pos.push_back(s.pos[i]);
	}

	this->alive = s.alive;

	return *this;
}

// Grows the snake, for use after a "fruit" goal is achieved.
void Snake::grow()
{
	pos.push_back(pos[pos.size() - 1]);
}

// Updates position based on direction state.  Used by the board class for displaying snake to window output.
void Snake::update()
{
	for (int i = pos.size() - 1; i > 0; i--)
	{
		pos[i].first = pos[i - 1].first;
		pos[i].second = pos[i - 1].second;
	}

	switch (direction)
	{
	case 'u':
		pos[0].second--;
		break;
	case 'd':
		pos[0].second++;
		break;
	case 'l':
		pos[0].first--;
		break;
	case 'r':
		pos[0].first++;
		break;
	default:
		break;
	}

	outOfBounds();
	didEatSelf();
}

// Checks for outOfBounds status.
bool Snake::outOfBounds()
{
    // Both conditions look for a position that is out of bounds.
    // If the position is out of bounds, then a fail state occurs via "alive = false".
	if (pos[0].first < 1 || pos[0].first > bounds)
	{
		alive = false;
		cout << "\n\nYOU WENT OUT OF BOUNDS\n\n";
	}
	else if (pos[0].second < 1 || pos[0].second > bounds)
	{
		alive = false;
		cout << "\n\nYOU WENT OUT OF BOUNDS\n\n";
	}

	return alive;
}

//  Similar to outofBounds, but instead checks for a position where the snake position is on top of its self.
bool Snake::didEatSelf()
{
    // Checks if the latest position is the same as any other part of the snake, and returns a fail state.
	for (int i = 1; i < pos.size(); i++)
	{
		if (pos[0].first == pos[i].first && pos[0].second == pos[i].second)
		{
			alive = false;
			cout << "\n\nYOU ATE YOURSELF!\n\tYOU CANNIBAL!\n\n";
			break;
		}
	}
	return alive;
}