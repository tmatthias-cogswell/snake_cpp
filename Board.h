#ifndef _BOARD_H
#define _BOARD_H

//Snake.h contains the Snake class, which is initialized to exist within a Board object.
#include "Snake.h"
#include <string>

class Board{
public:
    // Default Ctor
    Board();
    // Function Declarations

    // Update checks to make sure the Snake is still active, and calls placeFruit() if the Snake reaches a "fruit" goal.
    void update();
    // Places a new "Fruit" object, a goal for the snake.
    void placeFruit();
    //Generates a random location for placeFruit() to use as a location on the board grid.
    int randomPoint(){ return 1 + (rand() % (size - 2)); }
    // Updates display to the console/terminal window.
    void display();
    // Checks user keyboard input for snake direction.
    void parseInput(char c);
    // Returns the play state, used when the Snake has reached a fail state (dies).
    bool isPlaying(){ return playing; }

private:
    // Prevents size from changing and forces size within the class scope.
    const static int size = 18;
    std::string tiles[size][size];

    //Initializes a Snake object.
    Snake theSnake;

    // Uses std library called in Snake class.
    // <utility> included in Snake.h.
    std::pair<int, int> fruit;

    bool playing;
};

#endif