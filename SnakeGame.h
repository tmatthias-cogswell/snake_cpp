#ifndef _SNAKEGAME_H
#define _SNAKEGAME_H

//Include Board so the class can initialize a Board object;
#include "Board.h"

//Class blueprint for the game loop.
class SnakeGame{
public:
    //Ctor declaration with default values for sleepTime, and setting the initial play state to false.
	SnakeGame(){ sleepTime = 500; playing = false; }
    //Function Declarations
	void play();
	void getUserInput();

private:
	int sleepTime;
	bool playing;
    //Declaration of an initialized Board object.
	Board theBoard;
};

#endif