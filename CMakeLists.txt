cmake_minimum_required(VERSION 3.3)
project(Snake_CPP)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES
        main.cpp
        SnakeGame.cpp
        SnakeGame.h
        Snake.cpp
        Snake.h
        Board.cpp
        Board.h)

add_executable(Snake_CPP ${SOURCE_FILES})