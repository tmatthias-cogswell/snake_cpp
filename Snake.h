#ifndef _SNAKE_H
#define _SNAKE_H
#include <vector>
#include <utility>

//Class blueprint for functionality of the "Snake" actor.  To be used by "SnakeGame.h"
class Snake{
public:
    //Default Ctor
	Snake();
    //Ctor with custom size/location
	Snake(int x, int y);
    //Copy Ctor
	Snake& operator=(const Snake& s);

    // Updates a change in the snakes direction via user input.
	void changeDirection(int d){ direction = d; }

    // Grows the snake after reaching a "fruit" goal.
	void grow();
    // Updates the snakes direction and checks for fail states.
	void update();

    // Fail state if the snake reaches a border.
	bool outOfBounds();

    // Fail state if the snake runs into itself after updating/moving.
	bool didEatSelf();

    // Gets a position for part of the snakes body.
	int getXofPartI(int i){ return pos[i].first; }
	int getYofPartI(int i){ return pos[i].second; }

    // Gets the entire length of the snake's body.
	int getLength(){ return pos.size(); }

    // Gets the current direction.
	char getDirection(){ return direction; }

    // Checks to make sure the snake hasn't reached a fail state.
	bool isAlive(){ return alive; }

    //  Set and return bounds.
	void setBounds(int size){ bounds = size; }
	int getBounds(){ return bounds; }

    // Checks for a user enabled fail state.
	// true giveUp ==> false alive
	void gaveUp(bool b){ alive = !b; }

private:
	//holds each point for the body of the snake
	//pos.size() is the length of the snake
	std::vector<std::pair<int, int>> pos;

	/*std::vector is like a linked list.
	It is part of the STL (standard template library)
	A template is a class which takes in a data type as
	an argument when creating the class. This makes the
	template class dynamic and flexible for data types.
	for example, a vector can be created to hold ints, or
	floats, or doubles.
	I recommend that you look up vector on cplusplus.com
	to learn about this useful data structure.

	std::pair is a templated struct, which allows two different
	data members. These members can be the same or different
	data types. They are accessed using .first and .second
	*/

	// 'u' 'd' 'l' 'r'
	char direction;
	bool alive;
	int bounds;

};

#endif