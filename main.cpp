//
// A Custom Built "Snake" game for C++ Programming.
//

//Snakegame.h contains most of the gameplay loop code, and includes the other relevant gameplay files.
#include "SnakeGame.h"
#include <iostream>


using namespace std;

int main()
{
    //Initializes a SnakeGame object named "g"
	SnakeGame g;

    //Starts the objects gameplay loop
	g.play();

    //Windows Platform Specific Command to tell the Terminal Window not to close.
	system("pause");

    //Exit Code for Successful Run
	return 0;
}