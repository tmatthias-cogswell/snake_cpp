#include "SnakeGame.h"
#include <iostream>
#include <Windows.h>
#include <conio.h>

//Definition of gameplay functionality.
void SnakeGame::play()
{
    //Provide user with control operations.
	std::cout << "\nw: up\ts: down\na: left\td: right\n\n";
    //Display board to terminal/console window.
	theBoard.display();
    //Prevent Windows OS from closing the terminal/console window.
	system("pause");

    //Change initial play state to true, which starts gameplay.
	playing = true;

    //
    // This loop controls actual gameplay.
    // The system is able to continue updating the display regardless of user input.
    // When the user does use keyboard input, the gameplay loop is able to register input, and continue.
    //
	while (playing)
	{
		system("cls");

        //Function that can look for user input without stopping the gameplay loop.
		getUserInput();

        //Update and re-display game board to window.
		theBoard.update();
		theBoard.display();

		if (sleepTime > 0)
		{
            //Reduces initial sleep time amount.
			Sleep(sleepTime--);
		}

        //Checks to make sure the game hasn't reached a "Game Over" state.
		playing = theBoard.isPlaying();
	}
    //Once the gameplay while loop exits, the game defaults to a "Game Over" state.

    //Notify user of "Game Over" state.
	std::cout << "\nGAME OVER\n\n";
}

//Compiler/Platform Specific method for checking for keyboard input without stopping runtime.
void SnakeGame::getUserInput()
{
	//_kbhit == keyboard hit
	if (_kbhit())
	{
		//_getch() == getchar() == get input
		theBoard.parseInput(_getch());
	}
}