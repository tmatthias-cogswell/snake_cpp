#include "Board.h"
#include <iostream>
#include <ctime>
#include <string>

using namespace std;
// Example Game Board
/*
18 === 0-9, A-H. DEFAULT BOARD
          playable area = [1 <-> 16]
   0 1 2 3 4 5 6 7 8 9 A B C D E F G H
 0   - - - - - - - - - - - - - - - -
 1 |                                 |
 2 |                                 |
 3 |                                 |
 4 |                                 |
 5 |                                 |
 6 |                                 |
 7 |                                 |
 8 |                                 |
 9 |                                 |
 A |                                 |
 B |                                 |
 C |                                 |
 D |                                 |
 E |                                 |
 F |                                 |
 G |                                 |
 H   - - - - - - - - - - - - - - - -
*/

//Initializes the board.
Board::Board()
{
    srand(time(NULL));

    //Draws blank spaces
    for (int x = 0; x < size; x++)
    {
        for (int y = 0; y < size; y++)
        {
            tiles[x][y] = "  ";
        }
    }

    //Draws border of the grid
    for (int i = 1; i < size - 1; i++)
    {
        tiles[i][0] = " -";
        tiles[i][size - 1] = " -";
        tiles[0][i] = " |";
        tiles[size - 1][i] = " |";
    }

    // Creates snake.
    theSnake = Snake(randomPoint(), randomPoint());
    // Sets bounds to the match the border.
    theSnake.setBounds(size - 2);

    // Places first goal for the snake.
    placeFruit();

    // Sets playstate.
    playing = true;
}

// update() first updates the position of the snake on the board.
// Then, update() checks to make sure the snake is in a valid position, and whether or not a fruit was captured.
void Board::update()
{
    theSnake.update();

    playing = theSnake.isAlive();

    if (theSnake.getXofPartI(0) == fruit.first && theSnake.getYofPartI(0) == fruit.second)
    {
        theSnake.grow();
        placeFruit();
    }
}

// placeFruit() first finds a random position for the new fruit.
// Then places it, making sure not to place it where the snake already exists.
void Board::placeFruit()
{
    fruit.first = randomPoint();
    fruit.second = randomPoint();

    for (int i = 0; i < theSnake.getLength(); i++)
    {
        while (theSnake.getXofPartI(i) == fruit.first && theSnake.getYofPartI(i) == fruit.second)
        {
            fruit.first = randomPoint();
            fruit.second = randomPoint();
        }
    }
}

//
// display() interprets the snakes direction, and outputs movement to the board accordingly.
// The fruit goal is also displayed as a "*" character on the board, according to its position.
//
void Board::display()
{
    // gets the position of the last placed fruit, and outputs a character to the game board.
    tiles[fruit.first][fruit.second] = " *";

    // Handles user input for direction, and outputs to the game board.
    switch (theSnake.getDirection())
    {
        case 'u':
            tiles[theSnake.getXofPartI(0)][theSnake.getYofPartI(0)] = " ^";
            break;
        case 'd':
            tiles[theSnake.getXofPartI(0)][theSnake.getYofPartI(0)] = " v";
            break;
        case 'l':
            tiles[theSnake.getXofPartI(0)][theSnake.getYofPartI(0)] = " <";
            break;
        case 'r':
            tiles[theSnake.getXofPartI(0)][theSnake.getYofPartI(0)] = " >";
            break;
        default:
            break;
    }

    // Displays the "body" of the snake following the latest direction change.
    for (int i = 1; i < theSnake.getLength(); i++)
    {
        tiles[theSnake.getXofPartI(i)][theSnake.getYofPartI(i)] = " @";
    }

    // Updates empty spaces in the board.
    for (int y = 0; y < size; y++)
    {
        for (int x = 0; x < size; x++)
        {
            cout << tiles[x][y];
        }
        cout << endl;
    }

    for (int i = 0; i < theSnake.getLength(); i++)
    {
        tiles[theSnake.getXofPartI(i)][theSnake.getYofPartI(i)] = "  ";
    }
}

// Gets user input from the keyboard, used for interpreting changes in the snakes movement direction.
// Also looks for a "quit" state where the user ends the game loop prematurely via the "q" key.
void Board::parseInput(char c)
{
    // Each case (besides q) updates the snakes direction, and then breaks.
    // The "q" case stops the game loop and informs the user of a successful "exit".
    switch (c)
    {
        case 'q':
            playing = false;
            theSnake.gaveUp(!playing);
            cout << "\nEXITING\n";
            break;
        case 'a':
            theSnake.changeDirection('l');
            break;
        case 'w':
            theSnake.changeDirection('u');
            break;
        case 'd':
            theSnake.changeDirection('r');
            break;
        case 's':
            theSnake.changeDirection('d');
            break;
        default:
            break;
    }
}
